# About
The purpose of this repository is to centralize our vision of what passionate us the most - coding.

Here you would find links to the best practices we follow, such as styling, testing, doing a code review, etc.

Feel free to contribute!

## Partition
- [Common practices](./common.md)
- [Frontend department](./frontend.md)
- [C++](./cpp.md)
