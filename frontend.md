# Intro
As a team of Javascript developers, we should understand the complexity that it cost to maintain a project on longterm basis. Being non-typed language, we should pay attention to the basic logic, structure and complexity of the code, as it could become a mess faster that we imagine.

To solve these problems, we have applied some rules in each of our repositories: typed code with auto-formatting, auto-testing before pushing and more. In this section we'll discuss all these technics.

# Basic strategy
The basic strategy is to stop wasting time administrating config files of utility code. 

Read more: https://en.wikipedia.org/wiki/KISS_principle

# Project composition
Our frontend projects are created using [Create React App](https://reactjs.org/docs/create-a-new-react-app.html). It is a starter for React applications that lets us to avoid using an extra overhead of project bundler (webpack) configuration.

Smaller libraries are typed and use a pretty feature of Typescript that lets us to deliver the compiled code of the same structure as the source.

# Code style
When we develop something, it is ok to have a raw version of one functionality's code. However, when it comes to the maintainability, it is important to have a comprehensible structure with readable logic that your teammate would understand.

The global approach is called a self-explainable code, which reads like a book. Here we will speak about basics that englobe the self-explainable code approach in our projects.

## Import order logic
We simplify the import resolution via `"baseUrl": "src"` Typescript flag, so you could write `import from 'redux/modules/actions'` from any point of your application instead of writing a `../../` relative path. 

**Attention!! This won't work in libraries distributed via mirroring of the source code.**

Import order should fall down from the farthest to the closest scope, sorted alphabetically.

First we would import Node modules, then - external project dependencies, created by the external world, then - the internal dependencies, created by us. Then you would import the bussiness logic (normally, located in redux layer) and project constants, helpers and services. After all, you would import the files that are relative to the current file, starting with `./`

<img src="./frontend/import-logic-order.png"  width="500">

## Naming
## Code complexity
WIP, about one-way data flow
