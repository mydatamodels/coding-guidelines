## How to do a code review

In a big company, the biggest challenge is to manage plenty of processes and at the same time, staying agile, react to environmental changes.

This article will help you to understand how you should proceed with technical reviews continiously gaining in quality and without wasting time.

source: [https://google.github.io/eng-practices/review/reviewer/](https://google.github.io/eng-practices/review/reviewer/)
