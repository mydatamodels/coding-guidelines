# MDM C++ Style Guide

## C++ version

Code should target C++17.

Do not use non-standard extensions and consider portability before using features from C++17.

## Header files

Header files should have `.hpp` extension.

### Self-contained

All header files should be self-contained, so they don't depend on the context of where they are included to work correctly.

### The #pragma once guard

It's mandatory for every header file. Although not standard, it's implemented in all the most adopted compilers.

### Forward declarations

Avoid using forward declarations where possible. Instead, #include the headers you need.

### Inline functions

Define functions inline only when they are small (10 lines or fewer).

### Order of includes

Include headers in the following order, separating each non-empty group with one empty line: STL headers, headers of 3rd party libraries, MDM libraries headers, project headers, module headers, related header (for `.cpp`).

Example for a file named `Utilities.cpp`:

```
// STL
#include <variant>

// 3rd party
#include <symengine/expression.h>

// MDM
#include <mdmengine/engine.hpp>

// Project
#include "Module/Module.hpp"

// Module
#include "constants.hpp"

// Related
#include "Utilities.hpp"

```

## Implementation

Implementation files need to have `.cpp` extension.

Avoid repetitions using functions, composition, inheritance, etc.

## Scoping

### Namespaces

Place code in a namespace. Namespaces should have unique names based on the project name, and possibly its path. Do not use using-directives (e.g. using namespace foo). Do not use inline namespaces.

### Non-member, static member, global functions

Sometimes it is useful to define a function not bound to a class instance. Such a function can be either a static member or a nonmember function. Nonmember functions should not depend on external variables, and should nearly always exist in a namespace. Do not create classes only to group static member functions; this is no different than just giving the function names a common prefix, and such grouping is usually unnecessary anyway.

### Locals

Place a function's variables in the narrowest scope possible, and initialize variables in the declaration.

### Static and global variables

Objects with static storage duration are forbidden unless they are trivially destructible.

Initialize statics in headers using inline (C++17 only).

#### Common patterns

*  If you require a static, fixed collection, such as a set to search against or a lookup table, use `frozen` lib.
*  For global strings use `constexpr` in a `constants.hpp` file.
*  `thread_local` members have to be static.

## Classes

[The rule of three/five/zero](https://en.cppreference.com/w/cpp/language/rule_of_three)

Use `= default` if you have to define a constructors / destructor / assignment operators and you don't have custom operations to perform.

Avoiding exceptions in custom move constructors / move assignment operators and using noexcept keyword [takes full advantage of move semantics](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#c66-make-move-operations-noexcept).

Avoid virtual method calls in constructors and destructors.

Use `= delete` when you want to disable default behavior.

Use of the pattern `this->` to access members is forbidden, except when it makes sense to have a function argument with the same name of the member. Example:

```
void compute(size_t number) {
this->number = number; // -> OK
this->count += number; // -> BAD
```

### Constructors

Use of initializer list for members is mandatory, and arguments can have the same name of the members if there isn't a straightforward alternative.

Example:

```
Widget::Widget(size_t width, size_t length) : width(width), length(length) {} // -> OK

Widget::Widget(size_t _width, size_t _length) : width(_width), length(_length) {} // -> BAD
```

Use of initializer list for base constructor and overloaded constructors of the same class is mandatory.

Avoid initialization that can fail if you can't signal an error.

### Implicit conversions

Do not define implicit conversions. Use the explicit keyword for conversion operators and single-argument constructors.

### Structs vs. Classes

Use a struct only for passive objects that carry data; everything else is a class.

### Structs vs. Pairs and Tuples

Prefer to use a struct instead of a pair or a tuple whenever the elements can have meaningful names.

### Inheritance

*  Prefer composition over inheritance.
*  Prefer compile time inheritance over runtime one.
*  Use override keyword.
*  Define a virtual destructor.
*  Duplicated code in derived classes is forbidden.

### Access control

Make classes' data members private, unless they are constants.

For technical reasons, we allow data members to be protected when using Google Test.

### Declaration order

Group similar declarations together, placing public parts and overrides earlier.

## Functions

Functions should be short. If a function exceeds about 40 lines, think about whether it can be broken up without harming the structure of the program.

If you find yourself copying / pasting code, it's highly likely that you need to create a function.

When you modify a function, check its usage to verify if your modification could rise some unwanted side effect.

### Output parameters

Prefer return values over output parameters. If you need an output parameter, place the input parameters earlier.

### Arguments

If arguments are objects, pass them by `&` or `const&`. You must have a valid reason if you want to pass by value (copy).

You can pass by `&&` (rvalue reference) if you want to force a move, but you have to remember to use `std::move` in the function (argument passed by `&&` are lvalue references in the function's body).

Forwarding references (`T&&`) make it possible to write a generic function wrapper that forwards its arguments to another function, and works whether or not its arguments are temporary objects and/or const. This is called **perfect forwarding**.

`T&&` with `std::forward` is encouraged for setters and if you need to pass the argument to another function that supports forwarding.

When you modify a function, don't change the arguments const-ness unless you have a very (very) good reason.

### Overloading

You may overload a function when there are no semantic differences between variants. These overloads may vary in types, qualifiers, or argument count.

### Behavior

You have to pass enum classes with meaningful members to modify the function behavior. Bools are forbidden. Example:

```
enum class DoSlowStuff { no, yes };

void compute(DoSlowStuff behavior);

compute(DoSlowStuff::yes);
```

## Ownership and Smart Pointers

The owner of a dynamically allocated object is an object or function that is responsible for ensuring that it is deleted when no longer needed.

Smart Pointers are classes that act like pointers, e.g. by overloading the * and -> operators. Some smart pointer types can be used to automate ownership bookkeeping, to ensure these responsibilities are met. `std::unique_ptr` is a smart pointer type introduced in C++11, which expresses **exclusive ownership** of a dynamically allocated object; the object is deleted when the `std::unique_ptr` goes out of scope. It cannot be copied, but can be moved to represent ownership transfer. `std::shared_ptr` is a smart pointer type that expresses **shared ownership** of a dynamically allocated object. `std::shared_ptr`s can be copied; ownership of the object is shared among all copies, and the object is deleted when the last `std::shared_ptr` is destroyed.

`std::unique_ptr`s are preferred, simply because they're lighter objects.

Raw pointers are forbidden.

## Run-Time Type Information (RTTI)

Don't use it.

## Casting

Use C++-style casts like `static_cast<double>(doubleValue)`, or brace initialization for conversion of arithmetic types like `int64_t y = int64_t{1} << 42`. Do not use cast formats like `int y = (int)x` or `int y = int(x)` (but the latter is okay when invoking a constructor of a class type).

## Pre-increment and Pre-decrement

Use prefix form (++i) of the increment and decrement operators with iterators and other template objects.

## Use const

Use const to pass references that shouldn't be modified.

Using const on local references is neither encouraged nor discouraged (if the IDE complains, do what it says).

Using const on local variables or on non-reference function arguments is discouraged (high code noise, really low added value).

## `constexpr`

Use `constexpr` to define true constants or to ensure constant initialization.

Group constants in constants files as much as possible, using meaningful namespaces to further group them.

## Integer types

Use `size_t` for indices, int otherwise.

## Macros

Macros are forbidden.

## NULL

Use nullptr for pointers, and '\0' for chars (and not the 0 literal).

## Type deduction

Use auto wherever possible.

## Loops

Prefer std::transform, range-based for loops, ranges-v3 library zip, structured bindings.

## Lambda

Use lambda expressions where appropriate. Prefer explicit captures.

## Boost

Use Boost.

## Containers
When you are not sure if an element is present in the container, use `.at()`: it performs bounds check.

Example: vector index passed by function argument.

Anyway, use `[]` if you're sure that the element is present: it's faster.

Example: for loop with index < size.

## Naming

### General rules

*  Use names that describe the purpose or intent of the object.
*  Minimize the use of abbreviations.
*  Descriptiveness should be proportional to the name's scope of visibility (example: `size_t i` could be used only in a ~5 lines loop, but it's better to use `index`, anyway)
*  T for template is OK.
*  lhs and rhs are OK for binary operations arguments.

In English, adjectives usually go before the nouns they modify. When choosing a name, you can overlook this rule to create a visual similarity between semantically similar objects. For example:

```
Widget widget;
Widget widgetWhite('color::white'); // -> OK
Widget whiteWidget('color::white'); // -> NOT SO OK
```

Most important rule: **BE CONSISTENT**! Calling the same thing in multiple ways is confusing and error prone.

### Files

`CamelCase`, `.hpp / .cpp`.

Do not use filenames that already exist in `/usr/include`, such as `db.h`.

### Types

`CamelCase`

### Variable and data members

`camelCase`

Don't use the types in the variable name. If you need to, use [hungarian notation](https://en.wikipedia.org/wiki/Hungarian_notation).

Pointers should be named like `pName`.

### Constants

`snake_case` or `SNAKE_CASE`

### Functions

`camelCase`

### Namespaces

`snake_case`

## Comments

Use doxygen comments for classes and functions.

Giving sensible names to types and variables is much better than using obscure names that you must then explain through comments.

Start each file with license boilerplate.

### Implementation comments

In your implementation you should have comments in tricky, non-obvious, interesting, or important parts of your code.

Be brief. Provide higher level comments that describe why the code does what it does. Comments have to provide **added value**.

Don't:
*  state the obvious
*  describe things not strictly related to the code
*  write comments more obscure than the code they're describing
*  literally describe what code does, unless the behavior is non obvious to a reader who understands C++ well

## Formatting

Use the provided .clang-format and a modern IDE (CLion or QT Creator).

### Blank lines

Blank lines visually separate two thoughts.

Use them:
*  before and after a block (class, function, if-then-else, etc)
*  after a group of declarations
*  before a comment line (usually helps readability)
*  before a return
*  at the end of the file

Don't put a blank line after an opening `{` or before closing a `}`.
